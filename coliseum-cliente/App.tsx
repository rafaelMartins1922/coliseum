import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './src/pages/Login/index';
import Register from './src/pages/Register/index';
import Tabs from './src/pages/Tabs/index';
import EventDetails from './src/pages/EventDetails/index';

import AuthProvider from './src/contexts/auth';
import Comments from './src/pages/Comments';

const { Navigator, Screen } = createStackNavigator();

export default function App() {

  return (
    <NavigationContainer>
      <AuthProvider>
        <Navigator
          screenOptions={{
            headerShown: false
          }}
        >
          <Screen
            name='Tabs'
            component={Tabs}
          />
          <Screen
            name='Login'
            component={Login}
          />
          <Screen
            name='Register'
            component={Register}
          />
          <Screen
            name='EventDetails'
            component={EventDetails}
          />
          <Screen
            name='Comments'
            component={Comments}
          />
        </Navigator>
      </AuthProvider>
    </NavigationContainer>
  );
}
