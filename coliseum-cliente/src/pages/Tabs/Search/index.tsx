import React from 'react';

import { useForm, Controller } from 'react-hook-form';
import { TouchableOpacity } from 'react-native';

import { Input } from '../../styles';
import { Header, Content, InputBox, FilterBox, TextBox, SearchText } from './styles';

import { LinearGradient } from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/Feather';

interface EditSearch {
    text: string
}

export default function Search() {

    const { control, handleSubmit } = useForm({ mode: 'onTouched' });
    const onSubmit = (data: EditSearch) => { 
      alert(data.text)
    };

    return(
        <Content>
            <Header>
              <LinearGradient
                  colors={['#FF4D00', '#FF9345']}
                  start={[0,1]}
                  end={[1,0]}
                  style={{
                  position: 'absolute',
                  width: "100%",
                  height: "100%",
                  borderBottomRightRadius: 35,
                  borderBottomLeftRadius: 35,
                  }}
              />
                <InputBox>
                    <TouchableOpacity onPress={handleSubmit(onSubmit)}>
                      <Icon name="search" size={20} color="#535353" />
                    </TouchableOpacity>

                    <Controller
                        control={control}
                        render={({ onChange, value }) => (
                            <Input
                                placeholder='Pesquise aqui...'
                                onChangeText={(value:string) => onChange(value)}
                                value={value}
                            />
                        )}
                        name='text'
                        defaultValue=''
                    />
                </InputBox>
                <FilterBox>
                    <Icon name="sliders" size={24} color="#fff" />
                </FilterBox> 
            </Header>
        </Content>
    );
}
